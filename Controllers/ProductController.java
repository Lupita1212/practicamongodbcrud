package com.back.pruebaMongoDB.Controllers;

import com.back.pruebaMongoDB.Models.ProductModel;
import com.back.pruebaMongoDB.Services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apiBack/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");
        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK) ;
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProduct(@PathVariable String id){
        System.out.println("getProductId");
        System.out.println("El id del producto a buscar es: "+ id);

        Optional<ProductModel> result=this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
               ) ;
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel productModel){
        System.out.println("addProducts");
        System.out.println("El id del producto es: " +productModel.getId());
        System.out.println("La descripcion producto es: " +productModel.getDesc());
        return new ResponseEntity<>(this.productService.add(productModel), HttpStatus.CREATED) ;
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel productModel,@PathVariable String id ){
        System.out.println("updateProduct");
        System.out.println("El id del producto actualizar en el parametro URL: " +id);
        System.out.println("El id del producto actualizar es: " +productModel.getId());
        System.out.println("La descripcion producto es: " +productModel.getDesc());

        Optional<ProductModel> productToUpdate =this.productService.findById(id);

        if (productToUpdate.isPresent()){
            System.out.println("El producto actualizar se ha encontrado, actualizando: " +id);

            this.productService.update(productModel);
        }

        return new ResponseEntity<>(
                productModel,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND) ;
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deletePrduct(@PathVariable String id){
        boolean deleteProduct =this.productService.delete(id);
        return new ResponseEntity<>(
                deleteProduct ? "producto eliminado" : "producto no eliminado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND) ;

    }


}
